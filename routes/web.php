<?php

use App\Http\Controllers\DateController;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/daily_task',[TaskController::class, 'index'])->name('task.index');
Route::post('/daily_task/store',[TaskController::class, 'store'])->name('task.store');
Route::get('/daily_task/{id}/edit',[TaskController::class, 'edit'])->name('task.edit');
Route::post('/daily_task/{id}/update',[TaskController::class, 'update'])->name('task.update');
Route::get('/daily_task/{id}/delete',[TaskController::class, 'delete'])->name('task.delete');



// date time
Route::get('/date_time',[DateController::class,'index'])->name('date_time.index');
Route::post('/date_time/store',[DateController::class,'store'])->name('date_time.store');
Route::get('/date_time/{id}/delete',[DateController::class,'delete'])->name('date_time.delete');