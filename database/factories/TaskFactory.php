<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'day' => fake()->date(),
            'task' => fake()->name(),
            'start_time' => fake()->date(),
            'end_time' => fake()->date(),
            'status' => rand(1,0),
            'date_time_ID' => rand(100,200),
        ];
    }
}
