<?php

namespace App\Http\Controllers;

use App\Models\date_time;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class DateController extends Controller
{
    public function index(){
        $dates = date_time::all();

        return view('schedule_task.index',compact('dates'));
    }

    public function store(Request $request){
        
        // return response()->json($request->date_time);

        $validate = Validator::make($request->all(),[
            'date' => 'required|unique:date_times'
        ]);

        if($validate->passes()){

            $date_time = new date_time();
            $date_time->date = $request->date;
            $date_time->save();         

            return response()->json([
                'status' => true,
                'message' => 'Date success !',
            ]);

        } else {

            return response()->json([
                'status' => false,
                'errors' => $validate->errors(),
            ]);
            
        }
        
    
    }
    public function delete($id){
        
        $date_time = date_time::find($id);
         
        if(!$date_time){

            return response()->json('data not found');

        } else{
            $date_time->delete();
            return response()->json([
                'status' => true,
                'message' => 'Date time delete success !'
            ]);
        }
        

    }
}
