<?php

namespace App\Http\Controllers;

use App\Models\date_time;
use App\Models\Task;
// use App\Models\Task;
// use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;    
// use Illuminate\Support\Facades\Validator::make;


class TaskController extends Controller
{
    public function index(){

        $data = [];
        $tasks = Task::latest()->paginate(10);
        $dateTime = date_time::latest()->paginate(1);
        $data['tasks'] = $tasks;
        $data['date_time'] = $dateTime;

        // $date_time = DateTime::all();
        return view('schedule_task.index',$data);

    }
    
    public function store(Request $request){

        $rule = [
            'day' => 'required',
            'task' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
        ];


        $validate = Validator::make($request->all(),$rule);

        if($validate->passes()){
            
            $task = new Task();
            $task->day = $request->day;
            $task->task = $request->task;
            $task->start_time = $request->start_time;
            $task->end_time = $request->end_time;
            
            if(!empty($request->status == 'on' )){
                $new_value = 1; 
                $task->status = $new_value;
            }

            $task->date_time_ID = $request->date_time;
            $task->save();

            return response()->json([
                'status' => true,
                'message' => 'create succes!',
            ]);
        } else {
            return response()->json([
                'status' => false,
                'errors' => $validate->errors(),
            ]);
        }
        
    }
    public function edit($id){
        
        return response()->json([
            'status' => true,
            'id' => $id,
        ]);
 
        return view('welcome');
        
    }
    public function update($id){

        return response()->json($id);

    }

    public function delete($id){
        
        $task = Task::find($id);

        if(!$task){
            return response()->json('task not found');
        }
        else{
            $task->delete();
            return response()->json([
                'status' =>  true,
                'message' => 'Task Delete Success !',
            ]);
        }
    }
}
