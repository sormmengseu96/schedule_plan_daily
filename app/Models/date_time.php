<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class date_time extends Model
{
    use HasFactory;

    protected $fillable =  [
        'date_times',
    ];
}
