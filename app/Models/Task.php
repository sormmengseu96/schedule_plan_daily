<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $Fillable = [
        'day',
        'task',
        'start_time',
        'end_time',
        'status',
        'date_time_ID',
    ];
}
