@extends('layout.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <button type="button" class="btn btn-dark btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal" style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .75rem;">
                        <i class="bi bi-plus-circle-dotted"></i>
                        NEW TASK
                    </button>
                    <button type="button" class="btn btn-dark btn-sm" data-bs-toggle="modal" data-bs-target="#dateTime" style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .75rem;">
                        <i class="bi bi-calendar-plus"></i>
                        DATE TIME
                    </button>
                    <button type="button" class="btn btn-light btn-sm" data-bs-toggle="modal" data-bs-target="#list" onclick="{{ route('date_time.index') }}" style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .75rem;">
                        <i class="bi bi-list-ol"></i>
                    </button>
                    </div>
                    <div class="col-md-2">
                        <input type="email"  class="form-control form-control-sm" id="colFormLabelSm">
                    </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-hover" id="dataTable" width="110%" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col">#ID</th>
                        <th scope="col">DAY</th>
                        <th scope="col">TASK</th>
                        <th scope="col">START TIME</th>
                        <th scope="col">END TIME</th>
                        <th scope="col">STATUS</th>
                        <th scope="col">CREATE AT</th>
                        <th scope="col">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    @if ($tasks->IsNotEmpty())
                        @foreach ($tasks as $task)
                            <tr>
                                <td>
                                    <span class="badge text-bg-dark">
                                        {{ $task->id }}
                                    </span>
                                </td>
                                <td>{{ $task->day }}</td>
                                <td>{{ $task->task }}</td>
                                <td>{{ $task->start_time }}</td>
                                <td>{{ $task->end_time }}</td>
                                <td>
                                    <span class=" {{ ($task->status == 1 ? 'badge text-bg-dark' : 'badge text-bg-danger') }}">
                                        {{ $task->status == 1 ? 'complete' : 'working' }}
                                    </span>
                                </td>
                                <td>{{ $task->created_at->format(date('y-m-d')) }}</td>
                                <td>
                                    <div class="btn-group dropstart">
                                        <button type="button" class="btn btn-light dropdown-toggle btn-sm" data-bs-toggle="dropdown" aria-expanded="false" style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .75rem;">
                                            <i class="bi bi-gear"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                {{--  --}}
                                                <a class="dropdown-item" href="{{ route('task.edit',$task->id) }}"  data-bs-toggle="modal" data-bs-target="#task_update">
                                                    <i class="bi bi-pencil-square"></i>
                                                    update
                                                </a>
                                            </li>
                                            <li>
                                                <a class="dropdown-item" onclick="deleteID({{ $task->id }})">
                                                    <i class="bi bi-trash3"></i>
                                                    delete
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>No records</td>
                        </tr>
                    @endif

                </tbody>
            </table>
        </div>
        <div class="card-footer">
            {{-- {{ $tasks->links() }} --}}
        </div>
        {{-- modal task create --}}
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="FormTask" name="FormTask">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel">New Task</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="mb-3">
                                    <label for="day">Day:</label>
                                    <input type="date" class="form-control" id="day" name="day">
                                    <p></p>
                                </div>
                                <div class="mb-3">
                                    <label for="task">Task:</label>
                                    <textarea class="form-control" id="task" name="task"></textarea>
                                    <p></p>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label for="start-time">Start time:</label>
                                        <input type="time" class="form-control" placeholder="start time" name="start_time" id="start_time">
                                        <p></p>

                                    </div>
                                    <div class="col">
                                        <label for="end-time">End time:</label>
                                        <input type="time" class="form-control" placeholder="end time" name="end_time" id="end_time">
                                        <p></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label for="status">Status:</label>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault" name="status">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <label for="status">Month & Year</label>
                                        <select class="form-select" name="date_time">
                                            @foreach ($date_time as $month)
                                                <option value="{{ $month->id }}">{{$month->date_time}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-dark btn-sm" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger btn-sm">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- modal task update --}}
        <div class="modal fade" id="task_update" tabindex="-1" aria-labelledby="task_update" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="FormTask_edit" name="FormTask_edit">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel">Update Task</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="mb-3">
                                    <label for="day">Day:</label>
                                    <input type="date" class="form-control" id="day" name="day">
                                    <p></p>
                                </div>
                                <div class="mb-3">
                                    <label for="task">Task:</label>
                                    <textarea class="form-control" id="task" name="task"></textarea>
                                    <p></p>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label for="start-time">Start time:</label>
                                        <input type="time" class="form-control" placeholder="start time" name="start_time" id="start_time">
                                        <p></p>

                                    </div>
                                    <div class="col">
                                        <label for="end-time">End time:</label>
                                        <input type="time" class="form-control" placeholder="end time" name="end_time" id="end_time">
                                        <p></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label for="status">Status:</label>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault" name="status">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <label for="status">Month & Year</label>
                                        <select class="form-select" name="date_time">
                                            @foreach ($date_time as $month)
                                                <option value="{{ $month->id }}">{{$month->date_time}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-dark btn-sm" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger btn-sm">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- modal date time create --}}
        <div class="modal fade" id="dateTime" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="Form_date_time" name="Form_date_time">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel">New Date</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="mb-3">
                                    <label for="day">Date:</label>
                                    <input type="date" class="form-control" id="date" name="date">
                                    <p></p>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-dark btn-sm"
                                data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger btn-sm">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- modal list date time    --}}
        <div class="modal fade" id="list" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="FormTask" name="FormTask">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel">List Date</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">#ID</th>
                                        <th scope="col">MONTH YEAR</th>
                                        <th scope="col">CREATE AT</th>
                                        <th scope="col">STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($date_time->isNotEmpty())
                                        @foreach ($date_time as $date)
                                            <tr>
                                                <td>
                                                    <span class="badge text-bg-dark">
                                                        {{ $date->id }}
                                                    </span>
                                                </td>
                                                <td>{{ $date->date_time }}</td>
                                                <td>{{ $date->created_at }}</td>
                                                <td>
                                                    <div class="btn-group dropstart">
                                                        <button type="button" class="btn btn-light dropdown-toggle btn-sm" data-bs-toggle="dropdown" aria-expanded="false" style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .75rem;">
                                                            <i class="bi bi-gear"></i>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <a class="dropdown-item" href="#">
                                                                    <i class="bi bi-pencil-square"></i>
                                                                    update
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="dropdown-item" onclick="deleteDate({{ $date->id }})">
                                                                    <i class="bi bi-trash3"></i>
                                                                    delete
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>
                                                record not found
                                            </td>
                                        </tr>
                                    @endif
                                   
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                </div>
            </div>
        </div>
        

    </div>

@endsection

@section('js')
    <script>
        // task create
        $('#FormTask').submit(function(e) {
            // alert('hello');
            e.preventDefault();
            var element = $(this);
            $.ajax({
                type: "POST",
                url: "{{ route('task.store') }}",
                data: element.serializeArray(),
                dataType: "json",
                success: function(response) {
                    var errors = response['errors']
                    if (response['status'] == false) {
                        if (errors['day']) {
                            $('#day')
                                .addClass('is-invalid')
                                .siblings("p")
                                .addClass('invalid-feedback').html(errors['day']);

                        }
                        if (errors['task']) {
                            $('#task')
                                .addClass('is-invalid')
                                .siblings("p")
                                .addClass('invalid-feedback').html(errors['task']);
                        }
                        if(errors['start_time']){
                            $('#start_time')
                                .addClass('is-invalid')
                                .siblings("p")
                                .addClass('invalid-feedback').html(errors['start_time']);
                        }
                        if(errors['end_time']){
                            $('#end_time')
                                .addClass('is-invalid')
                                .siblings("p")
                                .addClass('invalid-feedback').html(errors['end_time']);
                        }
                        // alert('errors');
                    } else if(response['status'] == true){
                        Swal.fire({
                        title: "Task add success",
                        icon: "success",
                        confirmButtonColor: "#28c700",
                        confirmButtonText: "OK"
                        }).then((result) => {
                        if (result.isConfirmed) {
                                location.reload()
                            }
                        });  
                    }
                    $(document).ready(function () {
                        $('#day').on('input', function () {
                            if ($(this).val().trim() !== ''){
                                $(this).removeClass('is-invalid');
                                $(this).addClass('is-valid');
                            }
                        });
                        $('#task').on('input', function () {
                            if ($(this).val().trim() !== ''){
                                $(this).removeClass('is-invalid');
                                $(this).addClass('is-valid');
                            }
                        });
                        $('#start_time').on('input', function () {
                            if ($(this).val().trim() !== ''){
                                $(this).removeClass('is-invalid');
                                $(this).addClass('is-valid');
                            }
                        });
                        $('#end_time').on('input', function () {
                            if ($(this).val().trim() !== ''){
                                $(this).removeClass('is-invalid');
                                $(this).addClass('is-valid');
                            }
                        });
                    });

                }
            });
        });

        // date time create
        $('#Form_date_time').submit(function (e) { 
            e.preventDefault();
            var element = $(this);
            $.ajax({
                type: "POST",
                url: "{{ route('date_time.store') }}",
                data: element.serializeArray(),
                dataType: "json",
                success: function(response) {
                    var errors = response['errors'];
                    if(response['status'] == false){
                        if(errors['date']){
                            $('#date').addClass('is-invalid').siblings('p').addClass('invalid-feedback').html(errors['date']);
                        } 
                        $(document).ready(function () {
                            $('#date').on('input', function () {
                                if ($(this).val().trim() !== ''){
                                    $(this).removeClass('is-invalid');
                                    $(this).addClass('is-valid');
                                }
                            });
                        })
                    } else if(response['status'] == true){
                        Swal.fire({
                        title: "Date add success",
                        icon: "success",
                        confirmButtonColor: "#28c700",
                        confirmButtonText: "OK"
                        }).then((result) => {
                        if (result.isConfirmed) {
                        
                                location.reload()
                            }
                        });  
                      
                            
                    }

                }
            });
        });
        // task delete
        function deleteID(DelID){
            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, delete it!"
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                        type: "get",
                        url: "/daily_task/" + DelID + "/delete",
                        dataType: "Json",
                        success: function (Response) {
                            if(Response['status'] == true){
                                Swal.fire({
                                title: "Task delete success",
                                icon: "success",
                                confirmButtonColor: "#28c700",
                                confirmButtonText: "OK"
                                }).then((result) => {
                                if (result.isConfirmed) {
                                        location.reload()
                                    }
                                }); 
                                // location.reload();

                            }
                        }
                    });
                      
                    }
                });
        }
        // date delete
        function deleteDate (DelID) {
            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, delete it!"
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                        type: "get",
                        url: "/date_time/" + DelID + "/delete",
                        dataType: "Json",
                        success: function (Response) {
                            if(Response['status'] == true){
                                Swal.fire({
                                title: "Date delete success",
                                icon: "success",
                                confirmButtonColor: "#28c700",
                                confirmButtonText: "OK"
                                }).then((result) => {
                                if (result.isConfirmed) {

                                        location.reload()

                                    }
                                }); 
                              
                            }
                        }
                    });
                      
                    }
                });
        }
   

        $(document).ready(function() {

            var modal = document.getElementById('exampleModal');

            var closeBtn = modal.querySelector('.btn-close');

            closeBtn.addEventListener('click', function() {
                modal.style.display = 'none';
                location.reload();
            });

            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = 'none';
                    location.reload();
                }
            };
            var modal = document.getElementById('dateTime');

            var closeBtn = modal.querySelector('.btn-close');

            closeBtn.addEventListener('click', function() {
                modal.style.display = 'none';
                location.reload();
            });

            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = 'none';
                    location.reload();
                }
            };
    });
    </script>
@endsection
